import React, { useState } from "react";
import PolyGraph from "./components/PolyGraph";

import { v4 as uuidv4 } from "uuid";

function App() {
  const [newLabel, setNewLabel] = useState("");
  const [stats, setStats] = useState([
    { id: 1, label: "A", value: 100 },
    { id: 2, label: "B", value: 100 },
    { id: 3, label: "C", value: 100 },
    { id: 4, label: "D", value: 100 },
    { id: 5, label: "E", value: 100 },
    { id: 6, label: "F", value: 100 },
  ]);

  const changeValue = (event, change_stat) => {
    const statsCopy = stats.slice();
    const statsAfterChange = [];

    for (let stat of statsCopy) {
      if (stat.id === change_stat.id) {
        statsAfterChange.push({
          id: change_stat.id,
          label: change_stat.label,
          value: event.target.value,
        });
      } else {
        statsAfterChange.push(stat);
      }
    }
    setStats(statsAfterChange);
  };

  const addStat = (e) => {
    e.preventDefault();
    if (newLabel && newLabel.length < 3) {
      setStats([...stats, { id: uuidv4(), label: newLabel, value: 100 }]);
      setNewLabel("");
    } else if (newLabel.length > 2) {
      alert("Length of point must be less than 3");
    }
  };

  const removeStat = (sid) => {
    if (stats.length > 3) {
      setStats(stats.filter((stat) => stat.id !== sid));
    } else {
      alert("Can't delete more!");
    }
  };

  return (
    <div>
      <svg width={200} height={200}>
        <PolyGraph stats={stats} />
      </svg>
      <div className="stats flex-col">
        {stats.map((stat) => (
          <div key={stat.id} className="m-3">
            <button onClick={() => removeStat(stat.id)} className="p-3">
              X
            </button>
            <label>{stat.label}</label>
            <input
              type="range"
              value={stat.value}
              onChange={(e) => changeValue(e, stat)}
              min={0}
              max={100}
            />
            <span>{stat.value}</span>
          </div>
        ))}
      </div>
      <form id="add">
        <input
          type="text"
          className="m-5"
          value={newLabel}
          onChange={(e) => setNewLabel(e.target.value)}
        />
        <button type="submit" onClick={addStat}>
          Add a stat
        </button>
      </form>
      <pre id="raw">{JSON.stringify(stats, null, 2)}</pre>
    </div>
  );
}

export default App;
