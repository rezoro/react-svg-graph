import { useEffect, useState } from "react";
import { valueToPoint } from "../utils/valueToPoint";

const AxisLabel = (props) => {
  const { stat, index, total } = props;
  const [point, setPoint] = useState({
    x: "",
    y: "",
  });

  useEffect(() => {
    setPoint(valueToPoint(parseInt(stat.value) + 10, index, total));
  }, [total]);

  return (
    <text x={point.x} y={point.y}>
      {stat.label}
    </text>
  );
};

export default AxisLabel;
