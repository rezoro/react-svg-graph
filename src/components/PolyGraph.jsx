import { useEffect, useState } from "react";
import { valueToPoint } from "../utils/valueToPoint";

import AxisLabel from "./AxisLabel";

const PolyGraph = (props) => {
  const { stats } = props;

  const [points, setPoints] = useState("");

  useEffect(() => {
    let total = stats.length;

    setPoints(
      stats
        .map((stat, index) => {
          const { x, y } = valueToPoint(stat.value, index, total);
          return `${x},${y}`;
        })
        .join(" ")
    );
  }, [stats]);

  return (
    <g>
      <polygon points={points}></polygon>
      <circle cx="100" cy="100" r="80"></circle>
      {stats.map((stat, index) => (
        <AxisLabel
          key={stat.id}
          stat={stat}
          index={index}
          total={stats.length}
        />
      ))}
    </g>
  );
};

export default PolyGraph;
